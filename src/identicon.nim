# This is just an example to get you started. A typical binary package
# uses this file as the main entry point of the application.

import md5
import strutils
import cairo
import strformat
import parseopt

type
  HSL = object
    hue: float
    saturation: float
    lightness: float

  RGB = object
    red: float
    green: float
    blue: float

proc toRGB(hsl : HSL) : RGB =
  var max_val, min_val : float

  if hsl.lightness < 50:
    max_val = 2.55 * (hsl.lightness + hsl.lightness * (hsl.saturation / 100))
    min_val = 2.55 * (hsl.lightness - hsl.lightness * (hsl.saturation / 100))
  else:
    max_val = 2.55 * (hsl.lightness + (100 - hsl.lightness) * (hsl.saturation / 100))
    min_val = 2.55 * (hsl.lightness - (100 - hsl.lightness) * (hsl.saturation / 100))

  if 0 <= hsl.hue and hsl.hue < 60:
    result = RGB(
      red: max_val,
      green: (hsl.hue / 60) * (max_val - min_val) + min_val,
      blue: min_val
    )
  elif 60 <= hsl.hue and hsl.hue < 120:
    result = RGB(
      red: ((120 - hsl.hue) / 60) * (max_val - min_val) + min_val,
      green: max_val,
      blue: min_val
    )
  elif 120 <= hsl.hue and hsl.hue < 180:
    result = RGB(
      red: min_val,
      green: max_val,
      blue: ((hsl.hue - 120) / 60) * (max_val - min_val) + min_val
    )
  elif 180 <= hsl.hue and hsl.hue < 240:
    result = RGB(
      red: min_val,
      green: ((240 - hsl.hue) / 60) * (max_val - min_val) + min_val,
      blue: max_val
    )
  elif 240 <= hsl.hue and hsl.hue < 300:
    result = RGB(
      red: ((hsl.hue - 240) / 60) * (max_val - min_val) + min_val,
      green: min_val,
      blue: max_val
    )
  elif 300 <= hsl.hue and hsl.hue <= 360:
    result = RGB(
      red: max_val,
      green: min_val,
      blue: ((360 - hsl.hue) / 60) * (max_val - min_val) + min_val
    )


when isMainModule:
  const
    background = RGB(
      red: 241/255,
      green: 241/255,
      blue: 241/255
    )
  var
    width: int32 = 256
    height: int32 = 256
    surface = imageSurfaceCreate(FORMAT_ARGB32, width, height)
    ctx = surface.create
    matrix : array[0..4, array[0..4, int]]
    start_x, start_y: float
    p = initOptParser()
    output_file, username: string

  for kind, key, val in p.getopt:
    case kind
    of cmdEnd: doAssert(false)
    of cmdShortOption, cmdLongOption:
      if key == "output" or key == "o":
        output_file = val
      if key == "help" or key == "h":
        """
generates identicon.

Usage:
  identicon [options] <username>

Options:
  -h --help   Show this text.
  -o --output specifies name of png file.
        """.echo
    of cmdArgument:
      username = key

  if username.len == 0:
    "username is required.".echo
    quit(1)

  if output_file.len == 0:
    output_file = fmt"{username}.png"

  ctx.setSourceRGB(1, 1, 1)
  ctx.paint

  let digest = $toMD5(username)

  for i in 0..2:
    for j in 0..4:
      matrix[i][j] = ($digest[j*5+i]).parseHexInt mod 2

  matrix[4] = matrix[0]
  matrix[3] = matrix[1]

  let hsl = HSL(
    hue: digest[25..27].parseHexInt * 360 / 4095,
    saturation: 65 - digest[28..29].parseHexInt * 20 / 255,
    lightness: 75 - digest[30..31].parseHexInt * 20 / 255
  )

  let rgb = hsl.toRGB

  for i in 0..4:
    for j in 0..4:
      start_x = (4 - i) * width / 5
      start_y = j * height / 5
      if matrix[i][j] == 0:
        ctx.setSourceRGB(rgb.red/255.0, rgb.green/255.0, rgb.blue/255.0)
        ctx.rectangle(start_x, start_y, width/5, height/5)
        ctx.fill
      else:
        ctx.setSourceRGB(background.red, background.green, background.blue)
        ctx.rectangle(start_x, start_y, width/5, height/5)
        ctx.fill

  discard surface.writeToPng(output_file)
