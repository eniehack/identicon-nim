# Package

version       = "0.1.0"
author        = "Nakaya"
description   = "generate identicon"
license       = "Apache-2.0"
srcDir        = "src"
bin           = @["identicon"]


# Dependencies

requires "nim >= 1.4.6"
requires "cairo >= 1.1.1"
